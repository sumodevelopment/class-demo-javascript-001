// Dom Elements
const elFood = document.getElementById('food');
const elAddOrderBtn = document.getElementById('btn-add-order');
const elOrderItems = document.getElementById('order-items');
const elTotal = document.getElementById('total-value');
const elCustomerName = document.getElementById('customer-name');
const elPlaceOrderBtn = document.getElementById('place-order-btn');

elAddOrderBtn.addEventListener('click', function(event) {
    const currentFood = elFood.value;
    const [ name, price ] = extractItemValues( currentFood );

    currentOrder = updateOrder( currentOrder, name, price );
    orderTotal = updateTotal( orderTotal, price );

    render();
});

elPlaceOrderBtn.addEventListener('click', function(event){

    // Desctructure the value property and rename it to customerName.
    const { value: customerName } = elCustomerName;

    // use .trim() to remove space at the end.
    if (!customerName.trim()) {
        alert('Please enter a customer name for the order');
        return;
    }

    // Yay! 
    alert(`${customerName}'s order for ${orderTotal}Kr is on the way! 🚙`);

    reset();

});

// Variables
let currentOrder = [];
let orderTotal = 0;

// Functions
// 
/**
 * Re-render the DOM with the latest variable data.
 */
function render() {

    // Display the latest total.
    elTotal.innerText = orderTotal;
    
    // Reset the innerHTML of the order items
    elOrderItems.innerHTML = '';

    // Create a new <li> for all the items in the order.
    currentOrder.forEach((orderItem)=>{
        const elItem = document.createElement('li');
        elItem.className = 'list-group-item'
        elItem.innerText = `${orderItem.name} - ${orderItem.price}`;
        elOrderItems.appendChild( elItem );
    });

    // Select the first Option
    elFood.value = elFood.childNodes[1].value;
    /* NB Note:
     * childNodes breaks option into text + option. Therefore using index 0 -> childNodes[0]
     * Actually refers to the text node not the option node.
     */
}

/**
 * Reset the DOM and variables
 */
function reset() {
    // Reset the DOM
    elCustomerName.value = '';
    elOrderItems.innerHTML = '';
    elTotal.innerText = 0;
    // Reset Data variables.
    orderTotal = 0;
    currentOrder = [];
}

// Short functions to make life easier.
const extractItemValues = orderItem => orderItem.split(',');
const updateTotal = (currentTotal, newItemPrice) => currentTotal + parseInt(newItemPrice);
const updateOrder = (currentItems, name, price) => [ ...currentItems, new OrderItem( name, price )];
/*
 * POSSIBLE IMPROVEMENT:
 * Try to use the .reduce() hof (higher order function) to create a list like this:
 * 1 x Burger - 180
 * 2 x Øl - 190
 * 
 * and so on
 */


/**
 * Function constructor to create a order item.
 * @param { String } name 
 * @param { String } price 
 */
function OrderItem(name, price) {
    this.name = name;
    this.price = parseInt(price);
}

// Try to add some more properties to an order item. Be creative